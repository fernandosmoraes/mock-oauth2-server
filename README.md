## mock oauth2 server

## Introdução
Esse projeto tem como objetivo remover a dependência da interação com o servidor Oauth2 da Betha, mockando as respostas que o serviço real costuma responder.

É bastante útil para os seguintes casos:

* Desenvolver local sem a necessidade de chamar um serviço externo. Isso possibilita a interação com o serviço mesmo com a sua indisponibilidade.
* Facilitar os testes unitários, manipulando as respostas conforme as entradas. Por exemplo, a a partir de um token ```x```, configurado com o escopo ```y```, garantir que determinada api pode ser chamada.

## Configuração
A configuração do serviço é baseada em arquivo ```.yml```, informando as entradas e quais as respostas que serão retornadas.

Por exemplo, caso a necessidade é simplesmente ter uma resposta igual para qualquer token que seja utilizado, pode-se utilizar o curinga ```*```:

```yml
tokens:
  '*':
    user: fernandosm
    system: 79
    scopes:
      - admin
      - notifications
```

Porém, caso exista a necessidade de comportamentos diferentes para cada token, é possível descrever a resposta de forma individual:

```yml
tokens:
  '*':
    user: fernandosm
    system: 79
    scopes:
      - admin
      - notifications
  '67fdb8c7-38ca-4b1a-a9bc-5d96dd74bad9':
    user: fernandosm
    system: 79
    scopes:
      - admin
      - dashboard
  'a533dbe3-9058-4f0b-a945-1f4b3b8d9006':
    user: fernandosm
    system: 79
    scopes:
      - admin
      - test
    expired: true
```

No exemplo acima, quando for requisitado um token, a prioridade de retorno será um match entre o token solicitado e o token configurado, porém caso nenhum token seja encontrado, a configuração baseada no curinga será retornada.

## Execução
A forma mais fácil de executar o serviço é através da seguinte imagem Docker: ```fernandomoraes/mock-oauth2-server```

```sh
docker run -i -v /tmp/conf.yml:/usr/src/app/conf.yml -p 3000:3000 fernandomoraes/mock-oauth2-server
```

Onde ```/tmp/conf.ym``` é o local onde se encontra a configuração do serviço.

## Testes
Após a inicializacão do serviço, pode-se testar as chamadas para verificar se tudo está ok conforme esperado:

```sh
curl http://localhost:3000/auth/oauth2/tokeninfo?access_token=67fdb8c7-38ca-4b1a-a9bc-5d96dd74bad91222
```

