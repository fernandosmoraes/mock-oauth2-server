const moment = require('moment');

//os handlers respondem conforme a implementação atual do oauth server utilizado pela Betha
module.exports =  {
	tokenMissing: response => {
		return response.status(400).send({ error: 'error.badRequest', error_description: null });
	},
	tokenNotFound: response => {
		return response.status(404).send({ error: 'not_found' });
	},
	tokenExpired: response => {
		return response.status(410).send({
			error: 'token_expired',
			expired: true,
			notFound: false
		});
	},
	tokenOK: (response, auth) => {
		return response.send({
			scopes: auth.scopes,
			expired: false,
			notFound: false,
			expires_in: moment().add(2, 'hours').valueOf(),
			user: { 
				id: auth.user 
			},
			client: {
				attributes: {
					sistema: auth.system
				}
			}
		});
	}
};