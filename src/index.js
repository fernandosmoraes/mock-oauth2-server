const express = require('express');
const app = express();
const port = 3000;

const handlers = require('./response-handlers');
const config = require('./mock-config');

app.get('/auth/oauth2/tokeninfo', (req, res) => {
	const token = req.query.access_token;

	if (!token) {
		return handlers.tokenMissing(res);
	}

	const auth = config.tokens[token] || config.tokens['*'];

	if (!auth) {
		return handlers.tokenNotFound(res);
	}

	if (auth.expired) {
		return handlers.tokenExpired(res, auth);
	}

	return handlers.tokenOK(res, auth);
});

app.listen(port);

console.log(`starting mock oauth2 server on port ${port}`);
console.log(JSON.stringify(config, null, 1));