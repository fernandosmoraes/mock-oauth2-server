const yaml = require('js-yaml');
const fs = require('fs');

const config = yaml.safeLoad(fs.readFileSync('conf.yml', 'utf-8'));

module.exports = config;