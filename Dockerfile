FROM node:8.11.4-alpine

ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY ./package*.json ./

RUN npm install

COPY . ./

EXPOSE 3000

CMD ["npm", "start"]
